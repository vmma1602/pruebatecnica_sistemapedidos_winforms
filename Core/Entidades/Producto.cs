﻿namespace Core.Entidades
{
    public class Producto
    {
        public int IdProducto { get; set; }
        public string? ClaveProducto { get; set; }
        public string NombreProducto { get; set; }
        public string Descripcion { get; set; }
        public double? PrecioUnitario { get; set; }
    }
}
