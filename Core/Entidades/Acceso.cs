﻿

namespace Core.Entidades
{
    public class Acceso
    {
        public int Id { get; set; }
        public string NombreUsuario { get; set; }
        public string UsuarioAcceso { get; set; }
        public string Rol { get; set; }
        public string Token { get; set; }
    }
}
