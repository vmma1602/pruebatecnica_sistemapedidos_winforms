﻿

namespace Core.Entidades
{
    public class Rol
    {
        public int IdRol { get; set; }
        public string? NombreRol { get; set; }
    }
}
