﻿namespace Core.Entidades
{
    public class Usuario
    {
        public int IdUsuario { get; set; }
        public string? NombreUsuario { get; set; }
        public string UsuarioAcceso { get; set; }
        public string ClaveAcceso { get; set; }
        public string NombreRol { get; set; }
        public int FkRol { get; set; }
    }
}
