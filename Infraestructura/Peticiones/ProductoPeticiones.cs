﻿using Core.Entidades;
using Newtonsoft.Json;
using RestSharp;

namespace Infraestructura.Peticiones
{
    public class ProductoPeticiones
    {
        public async Task<IList<Producto>> DevolverProductos()
        {
            string url = BaseURL.Url + "/api/Producto/DevolverProductos";
            var restCliente = new RestClient();
            var restRequest = new RestRequest(url, Method.Get);
            var restResponse = await restCliente.ExecuteAsync(restRequest);
            var prods = JsonConvert.DeserializeObject<IList<Producto>>(restResponse.Content);
            return prods;
        }

        public async Task<Producto> AgregarProducto(Producto producto)
        {
            string url = BaseURL.Url + "/api/Producto/AgregarProducto";
            var restCliente = new RestClient();
            var restRequest = new RestRequest(url, Method.Post);
            restRequest.AddJsonBody(new
            {
                producto.ClaveProducto,
                producto.NombreProducto,
                producto.Descripcion,
                producto.PrecioUnitario
            });
            var restResponse = await restCliente.ExecuteAsync(restRequest);
            producto = JsonConvert.DeserializeObject<Producto>(restResponse.Content);
            return producto;
        }

        public async Task<Producto> EditarProducto(Producto producto, int productoId)
        {
            string url = BaseURL.Url + "/api/Producto/EditarProducto";
            var restCliente = new RestClient();
            var restRequest = new RestRequest(url, Method.Put);
            restRequest.AddJsonBody(new
            {
                producto.ClaveProducto,
                producto.NombreProducto,
                producto.Descripcion,
                producto.PrecioUnitario
            });
            restRequest.AddQueryParameter("productoId", productoId.ToString());
            var restResponse = await restCliente.ExecuteAsync(restRequest);
            producto = JsonConvert.DeserializeObject<Producto>(restResponse.Content);
            return producto;
        }
    }
}
