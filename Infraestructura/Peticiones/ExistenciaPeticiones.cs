﻿using Core.Entidades;
using Newtonsoft.Json;
using RestSharp;


namespace Infraestructura.Peticiones
{
    public class ExistenciaPeticiones
    {
        public async Task<IList<ProductoExistencia>> DevolverExistencias()
        {
            string url = BaseURL.Url + "/api/Existencia/DevolverExistencias";
            var restCliente = new RestClient();
            var restRequest = new RestRequest(url, Method.Get);
            var restResponse = await restCliente.ExecuteAsync(restRequest);
            var prods = JsonConvert.DeserializeObject<IList<ProductoExistencia>>(restResponse.Content);
            return prods;
        }


        public async Task<ProductoExistencia> EditarExistencia(ProductoExistencia exist, int existenciaId)
        {
            string url = BaseURL.Url + "/api/Existencia/EditarExistencia";
            var restCliente = new RestClient();
            var restRequest = new RestRequest(url, Method.Put);
            restRequest.AddJsonBody(new
            {
                exist.UnidadDeMedida,
                exist.Stock
            });
            restRequest.AddQueryParameter("existenciaId", existenciaId.ToString());
            var restResponse = await restCliente.ExecuteAsync(restRequest);
            var existencia = JsonConvert.DeserializeObject<ProductoExistencia>(restResponse.Content);
            return existencia;
        }
    }
}
