﻿using Core.Entidades;
using Newtonsoft.Json;
using RestSharp;

namespace Infraestructura.Peticiones
{
    public class UsuarioPeticiones
    {
        public async Task<IList<Usuario>> DevolverUsuarios()
        {
            string url = BaseURL.Url + "/api/Usuario/DevolverUsuarios";
            var restCliente = new RestClient();
            var restRequest = new RestRequest(url, Method.Get);
            var restResponse = await restCliente.ExecuteAsync(restRequest);
            var usuarios = JsonConvert.DeserializeObject<IList<Usuario>>(restResponse.Content);
            return usuarios;
        }

        public async Task<Usuario> AgregarUsuario(Usuario usuario)
        {
            string url = BaseURL.Url + "/api/Usuario/AgregarUsuario";
            var restCliente = new RestClient();
            var restRequest = new RestRequest(url, Method.Post);
            restRequest.AddJsonBody(new
            {
                usuario.NombreUsuario,
                usuario.UsuarioAcceso,
                usuario.ClaveAcceso,
                usuario.FkRol
            });
            var restResponse = await restCliente.ExecuteAsync(restRequest);
            usuario = JsonConvert.DeserializeObject<Usuario>(restResponse.Content);
            return usuario;
        }

        public async Task<Usuario> EditarUsuario(Usuario usuario, int usuarioId)
        {
            string url = BaseURL.Url + "/api/Usuario/EditarUsuario";
            var restCliente = new RestClient();
            var restRequest = new RestRequest(url, Method.Put);
            restRequest.AddJsonBody(new
            {
                usuario.NombreUsuario,
                usuario.UsuarioAcceso,
                usuario.ClaveAcceso,
                usuario.FkRol
            });
            restRequest.AddQueryParameter("usuarioId", usuarioId.ToString());
            var restResponse = await restCliente.ExecuteAsync(restRequest);
            usuario = JsonConvert.DeserializeObject<Usuario>(restResponse.Content);
            return usuario;
        }
    }
}
