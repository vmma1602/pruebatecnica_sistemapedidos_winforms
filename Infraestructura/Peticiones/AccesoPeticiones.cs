﻿using Core.Entidades;
using Newtonsoft.Json;
using RestSharp;

namespace Infraestructura.Peticiones
{
    public class AccesoPeticiones
    {
        public async Task<(bool, Acceso)> Autenticar(string usuarioAcceso, string claveAcceso)
        {
            string url = BaseURL.Url + "/api/Token/Autenticar";
            var restCliente = new RestClient();
            var restRequest = new RestRequest(url, Method.Post);
            restRequest.AddJsonBody(new { usuarioAcceso, claveAcceso });
            var restResponse = await restCliente.ExecuteAsync(restRequest);
            var acceso = JsonConvert.DeserializeObject<Acceso>(restResponse.Content);

            return (restResponse.IsSuccessful, acceso);
        }
    }
}
