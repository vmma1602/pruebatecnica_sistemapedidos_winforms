﻿using Core.Entidades;
using Newtonsoft.Json;
using RestSharp;

namespace Infraestructura.Peticiones
{
    public class RolPeticiones
    {
        public async Task<IList<Rol>> DevolverRoles()
        {
            string url = BaseURL.Url + "/api/Rol/DevolverRoles";
            var restCliente = new RestClient();
            var restRequest = new RestRequest(url, Method.Get);
            var restResponse = await restCliente.ExecuteAsync(restRequest);
            var roles = JsonConvert.DeserializeObject<IList<Rol>>(restResponse.Content);
            return roles;
        }
    }
}
