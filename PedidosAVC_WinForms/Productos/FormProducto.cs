﻿using Core.Entidades;
using Infraestructura.Peticiones;

namespace PedidosAVC_WinForms.Productos
{

    public partial class FormProducto : Form
    {
        private readonly Acceso _acceso;
        Producto _producto;
        private readonly ProductoPeticiones _productoPeticiones = new();
        public FormProducto()
        {
            InitializeComponent();
        }

        public FormProducto(Acceso acceso, Producto producto)
        {
            InitializeComponent();
            _acceso = acceso;
            _producto = producto;
        }

        private void FormProducto_Load(object sender, EventArgs e)
        {
            CargarInputs();
        }

        private void CargarInputs()
        {
            LimpiarFormulario();
            if (_producto.IdProducto != 0)
            {
                txtIdProducto.Text = _producto.IdProducto.ToString();
                txtClaveProducto.Text = _producto.ClaveProducto;
                txtNombreProducto.Text = _producto.NombreProducto;
                txtDescripcion.Text = _producto.Descripcion;
                txtPrecio.Text = _producto.PrecioUnitario.ToString();
                return;
            }
        }

        private void LimpiarFormulario()
        {
            txtIdProducto.ResetText();
            txtPrecio.ResetText();
            txtClaveProducto.ResetText();
            txtNombreProducto.ResetText();
            txtDescripcion.ResetText();

        }

        private void CargarDatosDelFormulario()
        {
            //Obtiene los valores de los inputs
            _producto.Descripcion = txtDescripcion.Text;
            _producto.ClaveProducto = txtClaveProducto.Text;
            _producto.NombreProducto = txtNombreProducto.Text;
            _producto.PrecioUnitario = double.Parse(txtPrecio.Text);
        }

        private async void btnGuardarProducto_Click(object sender, EventArgs e)
        {
            CargarDatosDelFormulario();
            if (_producto.IdProducto == 0)
            {
                var prod = await _productoPeticiones.AgregarProducto(_producto);
                if (prod != null)
                {
                    txtIdProducto.Text = prod.IdProducto.ToString();
                    MessageBox.Show("Agregado correctamente");
                }
                return;
            }

            var prodEditado = await _productoPeticiones.EditarProducto(_producto, _producto.IdProducto);
            if (prodEditado != null)
            {
                txtIdProducto.Text = prodEditado.IdProducto.ToString();
                MessageBox.Show("Editado correctamente");
            }
        }

        private void btnCancelarProducto_Click(object sender, EventArgs e)
        {
            ActiveForm.Show();
            Close(); //Cierra el modal secundario
            Dispose();
        }
    }
}
