﻿using Core.Entidades;
using Infraestructura.Peticiones;

namespace PedidosAVC_WinForms
{
    public partial class Login : Form
    {
        private AccesoPeticiones _accesoPeticion;
        public Login()
        {
            InitializeComponent();
        }

        private async void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                string usuario = txtUsuario.Text;
                string clave = txtClave.Text;

                if (string.IsNullOrEmpty(usuario) || string.IsNullOrEmpty(clave))
                {
                    lblErrorDeAcceso.Text = "Credenciales de acceso no válidas";
                    return;
                }

                _accesoPeticion = new AccesoPeticiones();
                var respuesta = await _accesoPeticion.Autenticar(usuario, clave);
                bool permisoConcedido = respuesta.Item1;
                Acceso datosDeAcceso = respuesta.Item2;

                if (!permisoConcedido)
                {
                    lblErrorDeAcceso.Text = "Credenciales de acceso no válidas";
                    return;
                }

                Hide();
                var menu = new frmMenu(datosDeAcceso);
                menu.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al conectarse al servidor de datos", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Login_Load(object sender, EventArgs e)
        {

        }
    }
}
