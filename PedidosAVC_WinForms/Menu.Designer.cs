﻿namespace PedidosAVC_WinForms
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlMenu = new System.Windows.Forms.TabControl();
            this.tabListaUsuarios = new System.Windows.Forms.TabPage();
            this.btnEditarusuario = new System.Windows.Forms.Button();
            this.btnAgregarUsuario = new System.Windows.Forms.Button();
            this.dtgUsuarios = new System.Windows.Forms.DataGridView();
            this.tabListaProductos = new System.Windows.Forms.TabPage();
            this.btnEditarProducto = new System.Windows.Forms.Button();
            this.btnAgregarProducto = new System.Windows.Forms.Button();
            this.dtgProductos = new System.Windows.Forms.DataGridView();
            this.tabListaExistencias = new System.Windows.Forms.TabPage();
            this.btnEditarExistencia = new System.Windows.Forms.Button();
            this.dtgExistencias = new System.Windows.Forms.DataGridView();
            this.btnSalir = new System.Windows.Forms.Button();
            this.lblInstrucciones = new System.Windows.Forms.Label();
            this.tabControlMenu.SuspendLayout();
            this.tabListaUsuarios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgUsuarios)).BeginInit();
            this.tabListaProductos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductos)).BeginInit();
            this.tabListaExistencias.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgExistencias)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlMenu
            // 
            this.tabControlMenu.Controls.Add(this.tabListaUsuarios);
            this.tabControlMenu.Controls.Add(this.tabListaProductos);
            this.tabControlMenu.Controls.Add(this.tabListaExistencias);
            this.tabControlMenu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControlMenu.Location = new System.Drawing.Point(0, 40);
            this.tabControlMenu.Name = "tabControlMenu";
            this.tabControlMenu.SelectedIndex = 0;
            this.tabControlMenu.Size = new System.Drawing.Size(886, 453);
            this.tabControlMenu.TabIndex = 0;
            // 
            // tabListaUsuarios
            // 
            this.tabListaUsuarios.Controls.Add(this.btnEditarusuario);
            this.tabListaUsuarios.Controls.Add(this.btnAgregarUsuario);
            this.tabListaUsuarios.Controls.Add(this.dtgUsuarios);
            this.tabListaUsuarios.Location = new System.Drawing.Point(4, 24);
            this.tabListaUsuarios.Name = "tabListaUsuarios";
            this.tabListaUsuarios.Padding = new System.Windows.Forms.Padding(3);
            this.tabListaUsuarios.Size = new System.Drawing.Size(878, 425);
            this.tabListaUsuarios.TabIndex = 1;
            this.tabListaUsuarios.Text = "Usuarios";
            this.tabListaUsuarios.UseVisualStyleBackColor = true;
            // 
            // btnEditarusuario
            // 
            this.btnEditarusuario.BackColor = System.Drawing.Color.Orange;
            this.btnEditarusuario.FlatAppearance.BorderSize = 0;
            this.btnEditarusuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditarusuario.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnEditarusuario.ForeColor = System.Drawing.Color.White;
            this.btnEditarusuario.Location = new System.Drawing.Point(693, 37);
            this.btnEditarusuario.Name = "btnEditarusuario";
            this.btnEditarusuario.Size = new System.Drawing.Size(177, 34);
            this.btnEditarusuario.TabIndex = 2;
            this.btnEditarusuario.Text = "Editar usuario";
            this.btnEditarusuario.UseVisualStyleBackColor = false;
            this.btnEditarusuario.Click += new System.EventHandler(this.btnEditarusuario_Click);
            // 
            // btnAgregarUsuario
            // 
            this.btnAgregarUsuario.BackColor = System.Drawing.Color.Green;
            this.btnAgregarUsuario.FlatAppearance.BorderSize = 0;
            this.btnAgregarUsuario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarUsuario.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnAgregarUsuario.ForeColor = System.Drawing.Color.White;
            this.btnAgregarUsuario.Location = new System.Drawing.Point(13, 37);
            this.btnAgregarUsuario.Name = "btnAgregarUsuario";
            this.btnAgregarUsuario.Size = new System.Drawing.Size(177, 34);
            this.btnAgregarUsuario.TabIndex = 1;
            this.btnAgregarUsuario.Text = "Agregar usuario";
            this.btnAgregarUsuario.UseVisualStyleBackColor = false;
            this.btnAgregarUsuario.Click += new System.EventHandler(this.btnAgregarUsuario_Click);
            // 
            // dtgUsuarios
            // 
            this.dtgUsuarios.AllowUserToAddRows = false;
            this.dtgUsuarios.AllowUserToDeleteRows = false;
            this.dtgUsuarios.AllowUserToOrderColumns = true;
            this.dtgUsuarios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgUsuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgUsuarios.Location = new System.Drawing.Point(13, 101);
            this.dtgUsuarios.Name = "dtgUsuarios";
            this.dtgUsuarios.ReadOnly = true;
            this.dtgUsuarios.RowTemplate.Height = 25;
            this.dtgUsuarios.Size = new System.Drawing.Size(859, 313);
            this.dtgUsuarios.TabIndex = 0;
            this.dtgUsuarios.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgUsuarios_CellClick);
            this.dtgUsuarios.MouseHover += new System.EventHandler(this.dtgUsuarios_MouseHover);
            // 
            // tabListaProductos
            // 
            this.tabListaProductos.Controls.Add(this.btnEditarProducto);
            this.tabListaProductos.Controls.Add(this.btnAgregarProducto);
            this.tabListaProductos.Controls.Add(this.dtgProductos);
            this.tabListaProductos.Location = new System.Drawing.Point(4, 24);
            this.tabListaProductos.Name = "tabListaProductos";
            this.tabListaProductos.Padding = new System.Windows.Forms.Padding(3);
            this.tabListaProductos.Size = new System.Drawing.Size(878, 425);
            this.tabListaProductos.TabIndex = 0;
            this.tabListaProductos.Text = "Productos";
            this.tabListaProductos.UseVisualStyleBackColor = true;
            // 
            // btnEditarProducto
            // 
            this.btnEditarProducto.BackColor = System.Drawing.Color.Orange;
            this.btnEditarProducto.FlatAppearance.BorderSize = 0;
            this.btnEditarProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditarProducto.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnEditarProducto.ForeColor = System.Drawing.Color.White;
            this.btnEditarProducto.Location = new System.Drawing.Point(692, 37);
            this.btnEditarProducto.Name = "btnEditarProducto";
            this.btnEditarProducto.Size = new System.Drawing.Size(177, 34);
            this.btnEditarProducto.TabIndex = 4;
            this.btnEditarProducto.Text = "Editar producto";
            this.btnEditarProducto.UseVisualStyleBackColor = false;
            this.btnEditarProducto.Click += new System.EventHandler(this.btnEditarProducto_Click);
            // 
            // btnAgregarProducto
            // 
            this.btnAgregarProducto.BackColor = System.Drawing.Color.Green;
            this.btnAgregarProducto.FlatAppearance.BorderSize = 0;
            this.btnAgregarProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarProducto.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnAgregarProducto.ForeColor = System.Drawing.Color.White;
            this.btnAgregarProducto.Location = new System.Drawing.Point(12, 37);
            this.btnAgregarProducto.Name = "btnAgregarProducto";
            this.btnAgregarProducto.Size = new System.Drawing.Size(177, 34);
            this.btnAgregarProducto.TabIndex = 3;
            this.btnAgregarProducto.Text = "Agregar producto";
            this.btnAgregarProducto.UseVisualStyleBackColor = false;
            this.btnAgregarProducto.Click += new System.EventHandler(this.btnAgregarProducto_Click);
            // 
            // dtgProductos
            // 
            this.dtgProductos.AllowUserToAddRows = false;
            this.dtgProductos.AllowUserToDeleteRows = false;
            this.dtgProductos.AllowUserToOrderColumns = true;
            this.dtgProductos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgProductos.Location = new System.Drawing.Point(12, 87);
            this.dtgProductos.Name = "dtgProductos";
            this.dtgProductos.ReadOnly = true;
            this.dtgProductos.RowTemplate.Height = 25;
            this.dtgProductos.Size = new System.Drawing.Size(860, 330);
            this.dtgProductos.TabIndex = 0;
            this.dtgProductos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgProductos_CellClick);
            this.dtgProductos.MouseHover += new System.EventHandler(this.dtgProductos_MouseHover);
            // 
            // tabListaExistencias
            // 
            this.tabListaExistencias.Controls.Add(this.btnEditarExistencia);
            this.tabListaExistencias.Controls.Add(this.dtgExistencias);
            this.tabListaExistencias.Location = new System.Drawing.Point(4, 24);
            this.tabListaExistencias.Name = "tabListaExistencias";
            this.tabListaExistencias.Size = new System.Drawing.Size(878, 425);
            this.tabListaExistencias.TabIndex = 2;
            this.tabListaExistencias.Text = "Existencias";
            this.tabListaExistencias.UseVisualStyleBackColor = true;
            // 
            // btnEditarExistencia
            // 
            this.btnEditarExistencia.BackColor = System.Drawing.Color.Orange;
            this.btnEditarExistencia.FlatAppearance.BorderSize = 0;
            this.btnEditarExistencia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditarExistencia.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnEditarExistencia.ForeColor = System.Drawing.Color.White;
            this.btnEditarExistencia.Location = new System.Drawing.Point(693, 25);
            this.btnEditarExistencia.Name = "btnEditarExistencia";
            this.btnEditarExistencia.Size = new System.Drawing.Size(177, 34);
            this.btnEditarExistencia.TabIndex = 5;
            this.btnEditarExistencia.Text = "Editar existencia";
            this.btnEditarExistencia.UseVisualStyleBackColor = false;
            this.btnEditarExistencia.Click += new System.EventHandler(this.btnEditarExistencia_Click);
            // 
            // dtgExistencias
            // 
            this.dtgExistencias.AllowUserToAddRows = false;
            this.dtgExistencias.AllowUserToDeleteRows = false;
            this.dtgExistencias.AllowUserToOrderColumns = true;
            this.dtgExistencias.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgExistencias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgExistencias.Location = new System.Drawing.Point(8, 82);
            this.dtgExistencias.Name = "dtgExistencias";
            this.dtgExistencias.ReadOnly = true;
            this.dtgExistencias.RowTemplate.Height = 25;
            this.dtgExistencias.Size = new System.Drawing.Size(862, 335);
            this.dtgExistencias.TabIndex = 0;
            this.dtgExistencias.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgExistencias_CellClick);
            this.dtgExistencias.MouseHover += new System.EventHandler(this.dtgExistencias_MouseHover);
            // 
            // btnSalir
            // 
            this.btnSalir.BackColor = System.Drawing.Color.White;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnSalir.ForeColor = System.Drawing.Color.SteelBlue;
            this.btnSalir.Location = new System.Drawing.Point(744, 11);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(130, 23);
            this.btnSalir.TabIndex = 1;
            this.btnSalir.Text = "Cerrar sesión";
            this.btnSalir.UseVisualStyleBackColor = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lblInstrucciones
            // 
            this.lblInstrucciones.AutoSize = true;
            this.lblInstrucciones.Location = new System.Drawing.Point(9, 12);
            this.lblInstrucciones.Name = "lblInstrucciones";
            this.lblInstrucciones.Size = new System.Drawing.Size(300, 15);
            this.lblInstrucciones.TabIndex = 2;
            this.lblInstrucciones.Text = "Para editar un registro, haz click y luego presiona editar.";
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(886, 493);
            this.Controls.Add(this.lblInstrucciones);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.tabControlMenu);
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.Load += new System.EventHandler(this.frmMenu_Load);
            this.tabControlMenu.ResumeLayout(false);
            this.tabListaUsuarios.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgUsuarios)).EndInit();
            this.tabListaProductos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgProductos)).EndInit();
            this.tabListaExistencias.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgExistencias)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TabControl tabControlMenu;
        private TabPage tabListaProductos;
        private TabPage tabListaUsuarios;
        private Button btnEditarusuario;
        private Button btnAgregarUsuario;
        private DataGridView dtgUsuarios;
        private TabPage tabListaExistencias;
        private Button btnEditarProducto;
        private Button btnAgregarProducto;
        private DataGridView dtgProductos;
        private Button btnEditarExistencia;
        private DataGridView dtgExistencias;
        private Button btnSalir;
        private Label lblInstrucciones;
    }
}