﻿namespace PedidosAVC_WinForms.Existencias
{
    partial class FormExistencia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbUnidadMedida = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStock = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCancelarExistencia = new System.Windows.Forms.Button();
            this.btnGuardarExistencia = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cmbUnidadMedida
            // 
            this.cmbUnidadMedida.FormattingEnabled = true;
            this.cmbUnidadMedida.Items.AddRange(new object[] {
            "pz",
            "l",
            "kg",
            "gr",
            "ton",
            "oz"});
            this.cmbUnidadMedida.Location = new System.Drawing.Point(33, 41);
            this.cmbUnidadMedida.Name = "cmbUnidadMedida";
            this.cmbUnidadMedida.Size = new System.Drawing.Size(192, 23);
            this.cmbUnidadMedida.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Unidad de medida";
            // 
            // txtStock
            // 
            this.txtStock.Location = new System.Drawing.Point(33, 91);
            this.txtStock.Name = "txtStock";
            this.txtStock.Size = new System.Drawing.Size(192, 23);
            this.txtStock.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Stock";
            // 
            // btnCancelarExistencia
            // 
            this.btnCancelarExistencia.BackColor = System.Drawing.Color.IndianRed;
            this.btnCancelarExistencia.FlatAppearance.BorderSize = 0;
            this.btnCancelarExistencia.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancelarExistencia.ForeColor = System.Drawing.Color.White;
            this.btnCancelarExistencia.Location = new System.Drawing.Point(33, 130);
            this.btnCancelarExistencia.Name = "btnCancelarExistencia";
            this.btnCancelarExistencia.Size = new System.Drawing.Size(75, 23);
            this.btnCancelarExistencia.TabIndex = 13;
            this.btnCancelarExistencia.Text = "Cerrar";
            this.btnCancelarExistencia.UseVisualStyleBackColor = false;
            this.btnCancelarExistencia.Click += new System.EventHandler(this.btnCancelarExistencia_Click);
            // 
            // btnGuardarExistencia
            // 
            this.btnGuardarExistencia.BackColor = System.Drawing.Color.Green;
            this.btnGuardarExistencia.FlatAppearance.BorderSize = 0;
            this.btnGuardarExistencia.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnGuardarExistencia.ForeColor = System.Drawing.Color.White;
            this.btnGuardarExistencia.Location = new System.Drawing.Point(150, 130);
            this.btnGuardarExistencia.Name = "btnGuardarExistencia";
            this.btnGuardarExistencia.Size = new System.Drawing.Size(75, 23);
            this.btnGuardarExistencia.TabIndex = 12;
            this.btnGuardarExistencia.Text = "Guardar";
            this.btnGuardarExistencia.UseVisualStyleBackColor = false;
            this.btnGuardarExistencia.Click += new System.EventHandler(this.btnGuardarExistencia_Click);
            // 
            // FormExistencia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(260, 168);
            this.ControlBox = false;
            this.Controls.Add(this.btnCancelarExistencia);
            this.Controls.Add(this.btnGuardarExistencia);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtStock);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbUnidadMedida);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormExistencia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edición de existencias";
            this.Load += new System.EventHandler(this.FormExistencia_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComboBox cmbUnidadMedida;
        private Label label1;
        private TextBox txtStock;
        private Label label2;
        private Button btnCancelarExistencia;
        private Button btnGuardarExistencia;
    }
}