﻿using Core.Entidades;
using Infraestructura.Peticiones;


namespace PedidosAVC_WinForms.Existencias
{
    public partial class FormExistencia : Form
    {
        private Acceso _acceso;
        ProductoExistencia _existencia;

        private readonly ExistenciaPeticiones _existenciaPeticiones = new();
        public FormExistencia()
        {
            InitializeComponent();
        }
        public FormExistencia(Acceso acceso, ProductoExistencia existencia)
        {
            InitializeComponent();
            _acceso = acceso;
            _existencia = existencia;
        }

        private void FormExistencia_Load(object sender, EventArgs e)
        {
            CargarInputs();
        }

        private void CargarInputs()
        {
            LimpiarFormulario();

            txtStock.Text = _existencia.Stock.ToString();
            cmbUnidadMedida.Text = _existencia.UnidadDeMedida;
        }

        private void LimpiarFormulario()
        {
            txtStock.ResetText();
            cmbUnidadMedida.ResetText();

        }

        private async void btnGuardarExistencia_Click(object sender, EventArgs e)
        {
            _existencia.UnidadDeMedida = string.IsNullOrEmpty(cmbUnidadMedida.SelectedItem.ToString()) ? "pz" : cmbUnidadMedida.SelectedItem.ToString();
            _existencia.Stock = double.Parse(txtStock.Text);

            var editado = await _existenciaPeticiones.EditarExistencia(_existencia, _existencia.IdExistencia);
            if (editado != null)
            {
                MessageBox.Show("Editado correctamente");
            }
        }

        private void btnCancelarExistencia_Click(object sender, EventArgs e)
        {
            ActiveForm.Show();
            Close(); //Cierra el modal secundario
            Dispose();
        }
    }
}
