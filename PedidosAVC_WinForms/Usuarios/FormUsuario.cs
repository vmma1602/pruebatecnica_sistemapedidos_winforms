﻿using Core.Entidades;
using Infraestructura.Peticiones;

namespace PedidosAVC_WinForms.Usuarios
{
    public partial class FormUsuario : Form
    {
        private readonly Acceso _acceso;
        Usuario _usuario;
        private readonly UsuarioPeticiones _usuarioPeticion = new();
        private readonly RolPeticiones _rolPeticiones = new();
        public FormUsuario()
        {
            InitializeComponent();
        }
        public FormUsuario(Acceso acceso, Usuario usuario)
        {
            InitializeComponent();
            _acceso = acceso;
            _usuario = usuario;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private async void FormUsuario_Load(object sender, EventArgs e)
        {
            LLenarDatosDeUsuario();
        }

        private void LLenarDatosDeUsuario()
        {
            LimpiarFormulario();
            if (_usuario.IdUsuario != 0)
            {
                txtIdUsuario.Text = _usuario.IdUsuario.ToString();
                txtClaveAcceso.Text = _usuario.ClaveAcceso;
                txtNombreUsuario.Text = _usuario.NombreUsuario;
                txtUsuarioAcceso.Text = _usuario.UsuarioAcceso;
                cmbRol.SelectedText = _usuario.NombreRol;
                return;
            }
        }

        private void LimpiarFormulario()
        {
            txtIdUsuario.ResetText();
            txtClaveAcceso.ResetText();
            txtNombreUsuario.ResetText();
            txtUsuarioAcceso.ResetText();
            CargarRoles();
        }

        private async Task CargarRoles()
        {
            cmbRol.DataSource = await _rolPeticiones.DevolverRoles();
            cmbRol.ValueMember = "IdRol";
            cmbRol.DisplayMember = "NombreRol";
        }
        private void CargarDatosDelFormulario()
        {
            //Obtiene los valores de los inputs
            _usuario.NombreUsuario = txtNombreUsuario.Text;
            _usuario.FkRol = int.Parse(cmbRol.SelectedValue.ToString());
            _usuario.UsuarioAcceso = txtUsuarioAcceso.Text;
            _usuario.ClaveAcceso = txtClaveAcceso.Text;
        }

        private async void btnGuardarUsuario_Click(object sender, EventArgs e)
        {
            CargarDatosDelFormulario();
            if (_usuario.IdUsuario == 0)
            {
                var usuario = await _usuarioPeticion.AgregarUsuario(_usuario);
                if (usuario != null)
                {
                    txtIdUsuario.Text = usuario.IdUsuario.ToString();
                    MessageBox.Show("Agregado correctamente");
                }
                return;
            }

            var usuarioEditado = await _usuarioPeticion.EditarUsuario(_usuario, _usuario.IdUsuario);
            if (usuarioEditado != null)
            {
                txtIdUsuario.Text = usuarioEditado.IdUsuario.ToString();
                MessageBox.Show("Editado correctamente");
            }
        }

        private void btnCancelarUsuario_Click(object sender, EventArgs e)
        {
            ActiveForm.Show();
            Close(); //Cierra el modal secundario
            Dispose();
        }
    }
}
