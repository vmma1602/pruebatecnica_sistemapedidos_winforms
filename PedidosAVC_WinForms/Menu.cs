﻿using Core.Entidades;
using Infraestructura.Peticiones;
using PedidosAVC_WinForms.Existencias;
using PedidosAVC_WinForms.Productos;
using PedidosAVC_WinForms.Usuarios;

namespace PedidosAVC_WinForms
{
    public partial class frmMenu : Form
    {
        private readonly Acceso _acceso;
        private Usuario usuario = new();
        private Producto producto = new();
        private ProductoExistencia existencia = new();

        UsuarioPeticiones _usuarioPeticion = new();
        ProductoPeticiones _productoPeticiones = new();
        ExistenciaPeticiones _existenciaPeticiones = new();
        public frmMenu()
        {
            InitializeComponent();
        }

        public frmMenu(Acceso acceso)
        {
            InitializeComponent();
            _acceso = acceso;
        }

        private async void frmMenu_Load(object sender, EventArgs e)
        {
            if (_acceso.Rol == "Administrador")
            {
                CargarDatosGridExistencias();
                CargarDatosGridProductos();
                CargarDatosGridUsuarios();
                return;
            }

            if (_acceso.Rol == "Vendedor")
            {
                tabControlMenu.TabPages.RemoveByKey("tabListaProductos");
                tabControlMenu.TabPages.RemoveByKey("tabListaUsuarios");
                btnEditarExistencia.Visible = false;
                return;
            }

            if (_acceso.Rol == "Administrativo")
            {
                tabControlMenu.TabPages.RemoveByKey("tabListaUsuarios");
                return;
            }
        }

        private async void CargarDatosGridUsuarios()
        {
            //Usuarios
            dtgUsuarios.DataSource = await _usuarioPeticion.DevolverUsuarios();
            dtgUsuarios.Columns[3].Visible = false;
            dtgUsuarios.Columns[5].Visible = false;   //Se ocultan columnas 
        }
        private async void CargarDatosGridProductos()
        {
            //Productos
            dtgProductos.DataSource = await _productoPeticiones.DevolverProductos();
        }

        private async void CargarDatosGridExistencias()
        {
            //Existencia
            dtgExistencias.DataSource = await _existenciaPeticiones.DevolverExistencias();
            dtgExistencias.Columns[0].Visible = false;
            dtgExistencias.Columns[3].Visible = false;   //Se ocultan columnas 
        }

        private void btnAgregarUsuario_Click(object sender, EventArgs e)
        {
            //Se abre un formulario limpio
            FormUsuario formUsuario = new(_acceso, new Usuario());
            formUsuario.ShowDialog();
        }

        private void dtgUsuarios_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int indexFilaActual = dtgUsuarios.CurrentRow.Index;
            usuario.NombreUsuario = dtgUsuarios["NombreUsuario", indexFilaActual].Value.ToString();
            usuario.NombreRol = dtgUsuarios["NombreRol", indexFilaActual].Value.ToString();
            usuario.UsuarioAcceso = dtgUsuarios["UsuarioAcceso", indexFilaActual].Value.ToString();
            usuario.IdUsuario = int.Parse(dtgUsuarios["IdUsuario", indexFilaActual].Value.ToString());

        }

        private void btnEditarusuario_Click(object sender, EventArgs e)
        {
            //Se abre un formulario con los datos del usuario
            FormUsuario formUsuario = new(_acceso, usuario);
            formUsuario.ShowDialog();
        }

        private async void dtgUsuarios_MouseHover(object sender, EventArgs e)
        {
            CargarDatosGridUsuarios();
        }

        private void dtgProductos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int indexFilaActual = dtgProductos.CurrentRow.Index;
            producto.ClaveProducto = dtgProductos["ClaveProducto", indexFilaActual].Value.ToString();
            producto.Descripcion = dtgProductos["Descripcion", indexFilaActual].Value.ToString();
            producto.NombreProducto = dtgProductos["NombreProducto", indexFilaActual].Value.ToString();
            producto.IdProducto = int.Parse(dtgProductos["IdProducto", indexFilaActual].Value.ToString());
            producto.PrecioUnitario = double.Parse(dtgProductos["PrecioUnitario", indexFilaActual].Value.ToString());
        }

        private void btnAgregarProducto_Click(object sender, EventArgs e)
        {

            FormProducto formProducto = new(_acceso, new Producto());
            formProducto.ShowDialog();
        }

        private void btnEditarProducto_Click(object sender, EventArgs e)
        {
            FormProducto formProducto = new(_acceso, producto);
            formProducto.ShowDialog();
        }

        private async void dtgProductos_MouseHover(object sender, EventArgs e)
        {
            CargarDatosGridProductos();
        }

        private void dtgExistencias_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int indexFilaActual = dtgExistencias.CurrentRow.Index;
            existencia.UnidadDeMedida = dtgExistencias["UnidadDeMedida", indexFilaActual].Value.ToString();
            existencia.IdExistencia = int.Parse(dtgExistencias["IdExistencia", indexFilaActual].Value.ToString());
            existencia.Stock = double.Parse(dtgExistencias["Stock", indexFilaActual].Value.ToString());
        }

        private void btnEditarExistencia_Click(object sender, EventArgs e)
        {
            FormExistencia formExistencia = new(_acceso, existencia);
            formExistencia.ShowDialog();
        }

        private async void dtgExistencias_MouseHover(object sender, EventArgs e)
        {
            CargarDatosGridExistencias();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Login login = new();
            login.Show();
            Close();
            Dispose();
        }
    }
}
